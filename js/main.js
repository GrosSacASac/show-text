import * as d from "../node_modules/dom99/built/dom99.es.js";
import { createDebounced } from "../node_modules/utilsac/utility.js";
import { translations, translationsLength } from "./translations.js";
import { rates } from "./exchangeRates.js";
import { getFast, getElseDefault, getElseSetDefault, setFast, clear, saveSilent, canUseLocalData }
    from "./localStorage.js";
import {
    compassSupport,
    start as compassStart,
    stop as compassStop
} from "./compass.js";
import { startServiceWorker } from "./service_worker/serviceWorkerManager.js";

const translations3 = getElseSetDefault(`translations`, {});
const translations2 = {};
Object.entries(translations).forEach(function ([original, translation]) {
    if (Object.prototype.hasOwnProperty.call(translations3, original)) {
        translations2[original] = translations3[original];
    } else {
        translations2[original] = {
            translation,
            favourite: true
        };
    }
});

let clickedText = "";
const select = function (event) {
    clickedText = event.target.textContent;
    const translated = translations[clickedText];
    if (!translated) {
        return; //missclickS
    }
    const translatedLength = Math.max(2, translated.length);
    //console.log(translatedLength);

    const favourite = translations2[clickedText].favourite === true;
    d.elements.removeFromFavourites.hidden = !favourite;
    d.elements.addAsFavourite.hidden = favourite;

    d.elements.bigDisplay.style.fontSize = `${75 / translatedLength}vmax`;
    d.elements.bigDisplay.style.lineHeight = `${80 / translatedLength}vmax`; // could be removed
    d.feed(`bigDisplay`, translated);
    d.feed(`original`, clickedText);
    d.elements.textContainer.hidden = false;
    d.elements.menu.hidden = true;
};

const goBack = function () {
    d.elements.menu.hidden = false;
    d.elements.textContainer.hidden = true;
};

// window.onerror = function (error) {
//     d.feed(`appError`, `Error ${error}`);
//     throw error;
// };

let lastChangedEuros = true;

const convertCore = function (event) {
    if (lastChangedEuros) {
        d.feed(`kw`, rates.rates.KRW * Number(d.variables.euros));
        return;
    }
    d.feed(`euros`, Number(d.variables.kw) / rates.rates.KRW);
};

const convert = function (event) {
    lastChangedEuros = (event.target === d.elements.euros);
    convertCore();
};

const onRatesChange = function () {
    convertCore();
    d.feed(`KRWFor1EUR`, rates.rates.KRW);
};

const saveNotes = function (event) {
    const now = new Date();
    const lastSaveNote = `saved at ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    setFast(`lastSaveNote`, lastSaveNote);
    setFast(`notes`, d.variables.notes);
    saveSilent();
    d.feed(lastSaveNote, `lastSaveNote`);
};


// here executes before dom99 went through
// here you cannot use d.elements
let compassEnabled = false;
const startCompass = function (event) {
    if (!compassEnabled) {
        d.elements.compass.hidden = false;

        d.feed(`Disable Compass`, `controlCompass`);
        compassStart();
    } else {
        d.elements.compass.hidden = true;

        d.feed(`Start Compass`, `controlCompass`);
        compassStop();

    }
    compassEnabled = !compassEnabled;

};

const removeFromFavourites = function (event) {
    translations2[clickedText].favourite = false;
    const wordsToTranslateHome =
        Object.entries(translations2).filter(function ([original, translationObject]) {
            return translationObject.favourite;
        }).map(function ([original]) {
            return original;
        });
    d.feed(`wordsToTranslate`, wordsToTranslateHome);
    translations3[clickedText] = translations2[clickedText];
    saveSilent();
    d.elements.removeFromFavourites.hidden = true;
    d.elements.addAsFavourite.hidden = false;
    event.preventDefault();
    event.stopPropagation();
    return false;

};

const addAsFavourite = function (event) {
    translations2[clickedText].favourite = true;
    const wordsToTranslateHome =
        Object.entries(translations2).filter(function ([original, translationObject]) {
            return translationObject.favourite;
        }).map(function ([original]) {
            return original;
        });
    d.feed(`wordsToTranslate`, wordsToTranslateHome);
    translations3[clickedText] = translations2[clickedText];
    saveSilent();
    d.elements.removeFromFavourites.hidden = false;
    d.elements.addAsFavourite.hidden = true;
    event.preventDefault();
    event.stopPropagation();
    return false;

};
const search = function (event) {
    const input = d.variables.search.toLowerCase();
    if (!input) {
        const wordsToTranslateHome =
            Object.entries(translations2).filter(function ([original, translationObject]) {
                return translationObject.favourite;
            }).map(function ([original]) {
                return original;
            });
        d.feed(`searchShownCount`, wordsToTranslateHome.length);
        d.feed(`wordsToTranslate`, wordsToTranslateHome);

    } else {
        const searchedList = Object.keys(translations).filter(function (original) {
            return original.includes(input);
        });
        d.feed(`searchShownCount`, searchedList.length);
        d.feed(`wordsToTranslate`, searchedList)
    }
};

const wordsToTranslate = Object.entries(translations2)
    .filter(function ([original, translationObject]) {
        return translationObject.favourite;
    }).map(function ([original]) {
        return original;
    });
d.start({
    dataFunctions: {
        search,
        removeFromFavourites,
        addAsFavourite,
        select,
        goBack,
        convert,
        saveNotes: createDebounced(saveNotes, 1000),
        startCompass
    }, // functions
    initialFeed: {
        wordsToTranslate,

        searchShownCount: wordsToTranslate.length,
        searchTotalCount: translationsLength,
        euros: `1`,
        // kw: convertCore is used
        //KRWFor1EUR: String(rates.rates.KRW),

        lastSaveNote: getElseDefault(`lastSaveNote`, ``),
        notes: getElseDefault(`notes`, ``)
    }, // initial feed
})

onRatesChange();
if (!canUseLocalData) {
    d.feed(`lastSave`, `localStorage is disabled, check your settings`);
    d.elements.notesFieldSet.disabled = true;
}
// function executes after dom99 went through
// here you can use d.elements
if (!compassSupport) {
    d.elements.compass.remove();
    d.elements.controlCompass.remove();
} else {

}

// d.elements.loadingHint.remove();
setTimeout(function () {
    startServiceWorker().then(function () {
        d.feed(`offline use ok`, `appInfo`);
    }).catch(function (reason) {
        d.feed(`offline use cannot be enabled, ${reason}`, `appInfo`);
    });
}, 3000);

export { onRatesChange };

