/*localStorage.js*/
/*
todo make storage more consistent when multiple tabs are open
    or give that responsability to the application
*/

const version = `2.0.0`;
const STORE_NAME = `show-text`.toUpperCase();
let canUseLocalData = Boolean(window.localStorage);
let fastCopy;

(function (itemName) {
    const getFreshStorage = {
        version,
        STORE_NAME
    };
    if (!canUseLocalData) {
        fastCopy = getFreshStorage;
        return;
    }
    try {
        const fastCopyString = localStorage.getItem(STORE_NAME);
        if (!fastCopyString) {
            // null or String
            // Empty
            fastCopy = getFreshStorage;
        } else {
            fastCopy = JSON.parse(fastCopyString);
            // we could also parse null directly for browsers that support the new JSON spec
            if (!fastCopy) {
                // Empty
                fastCopy = getFreshStorage;
            } else {

                if (fastCopy.version !== version) {
                    fastCopy = getFreshStorage;
                    // it resets everything but works
                    // could update things on a per field basis instead
                }
            }
        }

    } catch (e) {
        /* maybe there was no data,
        or stored differently before so it breaks JSON.parse
        clear all data and returns undefined */
        canUseLocalData = false;
        fastCopy = getFreshStorage;
    }
}());

const getFast = function (key) {
    if (Object.prototype.hasOwnProperty.call(fastCopy, key)) {
        return fastCopy[key];
    }
    return undefined;
};

const setFast = function (key, value) {
    fastCopy[key] = value;
    return value;
};

const getElseDefault = function (key, defaultValue) {
    const value = getFast(key);
    if (value === undefined) {
        return defaultValue;
    }
    return value;
};

const getElseSetDefault = function (key, defaultValue) {
    const value = getFast(key);
    if (value === undefined) {
        setFast(key, defaultValue);
        return getFast(key);
    }
    return value;
};


let saveSilent;
let clear;
if (canUseLocalData) {
    saveSilent = function () {
        localStorage.setItem(STORE_NAME, JSON.stringify(fastCopy));
        return true;
    };

    clear = function () {
        localStorage.removeItem(STORE_NAME);
        // localStorage.clear();
    };
} else {
    const doNothing = function () { };
    saveSilent = doNothing;
    clear = doNothing;
}

console.log(`Initial store from localStorage:`);
console.log(fastCopy);

export { getFast, getElseDefault, getElseSetDefault, setFast, clear, saveSilent, canUseLocalData };
