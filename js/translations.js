const translations = {
    "dormir": "수면",
    "manger": "먹다",
    "boire": "음주",
    "aller": "가기",
    "payer": "지불",
    "travailer": "일",
    "jouer": "놀이",
    "merci": "고맙습니다",
    "activités intéressantes": "재미있는 활동들",
    "lieux à visiter absoluments": "절대적으로 방문하는 장소",
};


const translationsLength = Object.keys(translations).length;
export { translations, translationsLength };
