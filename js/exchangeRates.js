
import { getFast, getElseDefault, getElseSetDefault, setFast, clear, saveSilent, canUseLocalData }
    from "./localStorage.js";
import { onRatesChange } from "./main.js";

const dayMilliseconds3 = 259200000; // 1000 * 60 * 60 * 24 * 3


const API_RATES = `https://RIP`;
const DISABLED = true;
/*
1.00 Danish Krone DKK =
0.13 Euro 
*/
const defaultRates = {
    "base": "EUR",
    "date": "2019-10-09",
    "rates": {
        "KRW": 1314.56
    }
}

let rates = getFast(`rates`);

const softRatesUpdate = function () {
    if (
        DISABLED ||
        !window.navigator ||
        !window.navigator.onLine ||
        !window.fetch
    ) {
        return;
    }
    fetch(API_RATES)
        .then(
            (response) => response.json()
        )
        .then((newRates) => {

            console.log("got new rate", newRates);
            rates = newRates;
            onRatesChange();
            // in case tab is kept open
            setTimeout(softRatesUpdate, dayMilliseconds3);
            if (canUseLocalData) {
                setFast(`rates`, rates);
                saveSilent();
            }
        })/*.catch(
      (error) => {console.log(, `no network`,error, `fallback to defaults`);}
  )*/;
};

if (!rates) {
    rates = defaultRates;
}

const lastUpdateTimeSinceEpoch = (new Date(rates.date)).getTime();
const nowTimeSinceEpoch = Date.now();
const moreThan3Day = (nowTimeSinceEpoch - lastUpdateTimeSinceEpoch) > dayMilliseconds3;

if (moreThan3Day) {
    softRatesUpdate();
} else {
    setTimeout(softRatesUpdate, dayMilliseconds3);
}


export { rates };
