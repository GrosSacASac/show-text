/*service_worker.js

*/
/*jslint
    es6, maxerr: 100, browser, devel, fudge, maxlen: 120, white
*/
/*global
    self, fetch, caches, Response, Request, Header,
*/

"use strict";

const SERVICE_WORKER_VERSION = `REPLACED_WITH_SERVICE_WORKER_VERSION`; // updated with tools/service_worker_version.js (String)
const CACHE_VERSION = SERVICE_WORKER_VERSION;

const HOME = "/";


const ressourcesToSaveInCache = `REPLACED_WITH_FILE_LIST`;


const fetchFromMainServer = function (request, options = {}) {
    /*wrap over fetch. The problem with fetch here, it doesn't reject properly sometimes
    see if statement below*/
    return fetch(request, options).then(function (fetchResponse) {
        if ((!fetchResponse) || (!fetchResponse.ok)) {
            return Promise.reject("fetch failed" + request.url);
        }
        return fetchResponse;
    });
};


const fetchFromCache = function (request) {
    return caches.open(CACHE_VERSION).then(function (cache) {
        return cache.match(request).then(function (CacheResponse) {
            if ((!CacheResponse) || (!CacheResponse.ok)) {
                return Promise.reject("Not in Cache");
            }
            return CacheResponse;
        });
    });
};

const fillServiceWorkerCache2 = function () {
    /*It will not cache and also not reject for individual resources that failed to be added in the cache. unlike fillServiceWorkerCache which stops caching as soon as one problem occurs. see http://stackoverflow.com/questions/41388616/what-can-cause-a-promise-rejected-with-invalidstateerror-here*/
    return caches.open(CACHE_VERSION).then(function (cache) {
        return Promise.all(
            ressourcesToSaveInCache.map(function (url) {
                return cache.add(url);
            })
        );
    });
};

const latePutToCache = function (request, response) {
    return caches.open(CACHE_VERSION).then(function(cache) {
        cache.put(request, response.clone());
        return response;
    });
};

const deleteServiceWorkerOldCache = function () {
    return caches.keys().then(function (cacheVersions) {
        return Promise.all(
            cacheVersions.map(function (cacheVersion) {
                if (CACHE_VERSION === cacheVersion) {
                    //console.log("No change in cache");
                } else {
                    //console.log("New SERVICE_WORKER_VERSION of cache, delete old");
                    return caches.delete(cacheVersion);
                }
            })
        );
    });
};

self.addEventListener("install", function (event) {
    /*the install event can occur while another service worker is still active

    waitUntil blocks the state (here installing) of the service worker until the
    promise is fulfilled (resolved or rejected). It is useful to make the service worker more readable and more deterministic

    save in cache some static ressources
    this happens before activation */
    event.waitUntil(
        fillServiceWorkerCache2()
        .then(skipWaiting)
    );
});

self.addEventListener("activate", function (event) {
    /* about to take over, other service worker are killed after activate, syncronous
    a good moment to clear old cache*/
    event.waitUntil(deleteServiceWorkerOldCache().then(function() {
        return self.clients.claim();
    }));
});

self.addEventListener("fetch", function (fetchEvent) {
    /* fetchEvent interface FetchEvent
    see https://www.w3.org/TR/service-workers/#fetch-event-interface
    IMPORTANT: fetchEvent.respondWith must be called inside this handler immediately
    synchronously fetchEvent.respondWith must be called with a response object or a
    promise that resolves with a response object. if fetchEvent.respondWith is called
    later in a callback the browser will take over and asks the remote server directly, do not do that

    why have fetchEvent.respondWith( and not respond with the return value of the callback function ?
    -->
    It allows to do other thing before killing the service worker, like saving stuff in cache
    */
    const request = fetchEvent.request;//Request implements Body;
    //const requestClone = request.clone(); //no need to clone ?
    const url = request.url;

    // Needs to activate to handle fetch


    if (request.method !== "GET") {
        return;
    }

    fetchEvent.respondWith(
        fetchFromCache(request.clone()).then(function (cacheResponse) {
            /* cannot use request again from here, use requestClone */
            return cacheResponse;
        }).catch(function (reason) {
            // We don't have it in the cache, fetch it
            return fetchFromMainServer(request);
        }).then(function (mainServerResponse) {
            return latePutToCache(request, mainServerResponse).catch(
                function (reason) {
                    /*failed to put in cache do not propagate catch, not important enough*/
                    return mainServerResponse;
                }
            );

        }).catch(function (reason) {
            return Promise.reject(reason);
        })
    );
});
