/*serviceWorkerManager.js
lives on the main thread
registers service_worker.js


*/
/*jslint
    maxerr: 100, browser, devel, fudge, maxlen: 120, white
*/
/*global
    window, navigator
*/


// true means maybe
// sw are disabled on private browsing
const serviceWorkerSupport = (
    window.navigator &&
    window.navigator.serviceWorker &&
    window.navigator.serviceWorker.register
);

// has to be in sync with scope
const serviceWorkerPath = "/service_worker.min.js";
let serviceWorkerRegistration;

const startServiceWorker = function () {
    if (!serviceWorkerSupport) {
        return Promise.reject(`No serviceWorker Support`);
    }
    //const options = {scope: "./"};
    return navigator.serviceWorker.register(serviceWorkerPath)
    // .then(function(registrationObject) {
    //     serviceWorkerRegistration = registrationObject;
    //     //console.log("service worker installed success!", registration);
    //
    //
    // }).catch(function(reason) {
    //     console.error("service worker could not install:", reason);
    // });


    // navigator.serviceWorker.addEventListener("activate", function (event) {
    //     console.log("serviceWorker: activate", location.origin, event);
    // });
    //
    // navigator.serviceWorker.addEventListener("controllerchange", function (event) {
    //     console.log("serviceWorker: controllerchange", event);
    // });

};

const deleteServiceWorker = function () {
    if (!serviceWorkerSupport) {
        return;
    }
    if (navigator.serviceWorker.getRegistrations) {
        navigator.serviceWorker.getRegistrations().then(function(registrations) {
            for(let registration of registrations) {
                registration.unregister().then(function(event) {
                    console.log("serviceWorker: unregistered", event);
                });
            }
        });
    } else if (navigator.serviceWorker.getRegistration) {
        // not plural
        navigator.serviceWorker.getRegistration().then(function(registration) {
            if (registration) {
                registration.unregister().then(function(event) {
                    console.log("serviceWorker: unregistered", event);
                });
            }
        });
    }

    // this could fail to remove something not yet active
    // serviceWorkerRegistration.unregister().then(function(event) {
        // console.log("serviceWorker: unregistered", event);
    // });
};

export {
    serviceWorkerSupport,
    startServiceWorker,
    deleteServiceWorker
};
