import * as d from "../node_modules/dom99/built/dom99.es.js";

const compassSupport = window.ondeviceorientation !== undefined; // should be null or function


const setDegreeAngle = function (dom, degrees) {
    dom.style.transform = `rotate(${degrees}deg)`;
};

function rotateToFindNorth(event) {
    //const directions = document.getElementById("directions");
    //if (evt.alpha < 5 || evt.alpha > 355) {
    //directions.innerHTML = "North!";
    //} else if (evt.alpha < 180) {
    //directions.innerHTML = "Turn left";
    //} else {
    //directions.innerHTML = "Turn right";
    //}
    setDegreeAngle(d.elements.compass, event.alpha);
};

const start = function () {
    window.addEventListener("deviceorientation", rotateToFindNorth);

};
const stop = function () {
    window.removeEventListener("deviceorientation", rotateToFindNorth);
};

export {
    compassSupport,
    start,
    stop
};
