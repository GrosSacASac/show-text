import { textFileContent, writeTextInFile, copyFile } from "filesac";

import { readdir } from "fs";


const dist = `./dist/`;
const originalSW = `./js/service_worker/service_worker.js`;
const builtSW = `${dist}service_worker.min.js`


readdir(dist, (err, files) => {
    // files.forEach(file => {
    //   console.log(file);
    // });
    const setOfFiles = new Set(files);
    setOfFiles.delete("index.html");
    setOfFiles.add("/");
    const filesAsString = JSON.stringify(Array.from(setOfFiles));
    const version = `version:${Date.now() / 12}`;
    // console.log(filesAsString);
    textFileContent(originalSW).then(function (originalServiceWorkerString) {
        let newServiceWorkerString = originalServiceWorkerString;
        newServiceWorkerString = newServiceWorkerString.replace(
            `REPLACED_WITH_SERVICE_WORKER_VERSION`,
            version
        );
        newServiceWorkerString = newServiceWorkerString.replace(
            `\`REPLACED_WITH_FILE_LIST\``,
            filesAsString
        );
        writeTextInFile(builtSW, newServiceWorkerString)

    });

});
